import configparser
import base64
import sys
import socket
import select
import os
import signal

os.system("clear")



def sigint_handler(signum, frame):
    print ('\n user interrupt ! shutting down')
    print ("[info] shutting down NEURON \n\n")
    sys.exit()


signal.signal(signal.SIGINT, sigint_handler)

#ask through command line ip-address and port for the socket connection
HOST = input("insert IP-address -> ")
PORT = int(input("insert port number -> "))
SOCKET_LIST = []
RECV_BUFFER = 4096


def chat_server():
    #create the socket server
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((HOST, PORT))
    server_socket.listen(2)

    SOCKET_LIST.append(server_socket)
    os.system('clear')
    print("********************************\n****   ENCHAT  SERVER  ON   ****\n********************************")
    print("\nAdress: " + HOST + "\nPort: " + str(PORT))
    while 1:

        ready_to_read, ready_to_write, in_error = select.select(
            SOCKET_LIST, [], [], 0)

        for sock in ready_to_read:

            if sock == server_socket:
                sockfd, addr = server_socket.accept()
                SOCKET_LIST.append(sockfd)

                #when they are two client, send them handshake message to start the process
                if len(SOCKET_LIST)>2:
                    broadcast(server_socket,sock,b"31732$t@rth@ndsh@k31732")

            else:
                #exchange messages or remove clients from the socket list in case of disconnection
                try:

                    data = sock.recv(RECV_BUFFER)
                    if data:
                        broadcast(server_socket, sock, data)
                    else:
                        if sock in SOCKET_LIST:
                            SOCKET_LIST.remove(sock)

                        broadcast(server_socket, sock,
                                  "user (%s, %s) is offline\n")

                except:
                    broadcast(server_socket, sock,
                              "user (%s, %s) is offline\n")
                    continue

    server_socket.close()

#broadcast function to send messages between clients
def broadcast(server_socket, sock, message):
    for socket in SOCKET_LIST:
        if socket != server_socket and socket != sock:
            try:
                socket.send(message)

            except:

                socket.close()

                if socket in SOCKET_LIST:
                    SOCKET_LIST.remove(socket)


if __name__ == "__main__":

    sys.exit(chat_server())

import os
import sys
from Crypto.Cipher import PKCS1_OAEP
from cryptography.fernet import Fernet
 

#function which generate a symmetric key and encrypt it with the public asymmetric one       
def encryptKey(public): 
    key =Fernet.generate_key()
    encryptor = PKCS1_OAEP.new(public)
    encrypted = encryptor.encrypt(key)
    return encrypted , key        
   

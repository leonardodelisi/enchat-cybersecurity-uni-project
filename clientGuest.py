# Enchat Guest Client

import base64
import sys
import socket
import select
import os
import signal
from cryptography.fernet import Fernet
from os import stat
from Crypto.PublicKey import RSA
from handshakeGuest import encryptKey


def sigint_handler(signum, frame):
    print ('\n user interrupt ! shutting down')
    print ("[info] shutting down EnChat \n\n")
    sys.exit()


signal.signal(signal.SIGINT, sigint_handler)


#ecrypt function with symmetric key for outgoing messages
def encrypt(secret, data):
    f = Fernet(secret)
    encrypted = f.encrypt(data.encode())
    return encrypted

#decrypt function with symmetric key for incoming messages
def decrypt(secret, data):
    f = Fernet(secret)
    decrypted = f.decrypt(data)
    return decrypted.decode()


os.system("clear")



def chat_client():
    key = ""
    if(len(sys.argv) < 5):
        print ('Usage : python3 clientGuest.py <hostname> <port> <password> <nick_name>')
        sys.exit()

    #setting global constants
    host = sys.argv[1]
    port = int(sys.argv[2])
    password = sys.argv[3]
    uname = sys.argv[4]

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)

    try:
        #connect client to the server if ip and port match
        s.connect((host, port))
        print('Wait the client Host before texing please...')

    except:
        print ("\033[91m"+'Unable to connect to the server'+"\033[0m")
        sys.exit()

    while 1:
        socket_list = [sys.stdin, s]
        read_sockets, write_sockets, error_sockets = select.select(
            socket_list, [], [])

        for sock in read_sockets:
            if sock == s:

                data = sock.recv(4096)

                if not data:
                    print (
                        "\033[91m"+"\nDisconnected from chat server"+"\033[0m")
                    sys.exit()
                else:
                    # start handshake process with asymmetric keys
                    if data == b"31732$t@rth@ndsh@k31732":
                        public_msg = sock.recv(4096)
                        public = RSA.importKey(public_msg)
                        encKey, key = encryptKey(public)
                
                        sock.send(encKey)

                        passHost = sock.recv(4096)
                        sock.send(encrypt(key,password))

                        #check if both clients have the same password
                        if decrypt(key,passHost) == password:
                            print ("\n\nHandshake and password verification complete!\nYou can start texing\n\n")
                            print("********************\n****   ENCHAT   ****\n********************")
                            print('\nUsername: ' + uname + '\n\n\n')
                            sys.stdout.write("\033[34m"+'\n[Me :] ' + "\033[0m")
                            sys.stdout.flush()
                        else:
                            #disconnect from server in case passsword are different
                            os.system("clear")
                            print("Passwords don't match" )
                            print ("\033[91m"+"\nDisconnected from chat server"+"\033[0m")
                            sys.exit()    
                       
                    else:
                        #decrypt incoming messages with the symmetric key and print them on the screen
                        data = decrypt(key, data)
                        sys.stdout.write('\r' + data)
                        sys.stdout.write("\033[34m"+'\n[Me :] ' + "\033[0m")
                        sys.stdout.flush()
                       


            else:
                #encrypt messages just written on the line command and send them throuth the server
                msg = sys.stdin.readline()
                msg = '[ ' + uname + ': ] '+msg
                msg = encrypt(key, msg)
                s.send(msg)
                sys.stdout.write("\033[34m"+'\n[Me :] ' + "\033[0m")
                sys.stdout.flush()


if __name__ == "__main__":

    sys.exit(chat_client())

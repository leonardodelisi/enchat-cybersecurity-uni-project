# EnChat App 

**Overview**

EnChat is a simple encrypted chat application written in python that allow users to chat using the Hybrid Encryption.


**Run Application**

1. `Install Python3` 
2. `Install pip3`, the package installer for Python3
3. Install libraries written below with this command `pip3 iinstall < library name >`:
    - pycryptodome
    - cryptography
    - socket
    - signal
    - select
4. Run the server with the command `python3 server.py` and choose ip-address and port
5. Run clients 
    - For Host client, run `python3 clientHost.py <hostname> <port> <password> <nick_name>`and choose the same previous ip-address and port, a password that the other client knows and a nickname for the chat.
    - For Guest client, run `python3 clientGuest.py <hostname> <port> <password> <nick_name>` with the same previous ip-address and port; the password must be the same of the host one.

6. Start Chatting!    
7. Press \textit{ctrl+c} to stop one of the ran script and end stop the connection (if you stop the server part, will be impossible for clients to connect again until the server will be on again.)





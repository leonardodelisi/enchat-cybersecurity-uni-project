import sys
from Crypto import Random
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP

#generate and return asymmetric pair of keys
def genKeys():
    random_generator = Random.new().read
    key = RSA.generate(1024,random_generator)
    public = key.publickey().exportKey()


    return public, key,

#decrypt the symmetric key received with the private one generated before    
def decryptor(private,msg):

    decryptor = PKCS1_OAEP.new(private)
    decrypted = decryptor.decrypt(msg)
    return decrypted.decode('utf-8')

   